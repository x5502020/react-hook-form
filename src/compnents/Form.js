import { useForm } from "react-hook-form";
// Styles
import "./Form.css";

const Form = () => {
  const {
    handleSubmit,
    register,
    reset,
    formState: { isDirty, isSubmitted, errors },
    watch,
  } = useForm();

  const watchFields = watch([
    "firstname",
    "lastname",
    "age",
    "employed",
    "favoriteColor",
    "sauses.Ketchup",
    "sauses.Mustard",
    "sauses.Mayonnaise",
    "sauses.Guacamole",
    "bestStooge",
    "note",
  ]);

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={`form-group`}>
          <label>First Name</label>
          <input
            style={{ outline: "none" }}
            className={isSubmitted && errors.firstname ? "invalid-field" : ""}
            type="text"
            id="firstname"
            {...register("firstname", {
              required: {
                value: true,
                message: "Username is not valid",
              },
              pattern: {
                value: /^[A-Za-z\s]+$/,
              },
            })}
          />
        </div>

        <div className="form-group">
          <label>Last Name</label>
          <input
            className={isSubmitted && errors.lastname ? "invalid-field" : ""}
            type="text"
            id="lastname"
            {...register("lastname", {
              required: {
                value: true,
                message: "Lastname is not valid",
              },
              pattern: /^[A-Za-z\s]+$/,
            })}
          />
        </div>

        <div className="form-group">
          <label>Age</label>
          <input
            className={isSubmitted && errors.age ? "invalid-field" : ""}
            id="age"
            type="number"
            {...register("age", {
              pattern: {
                value: /^[0-9]+$/,
              },
            })}
          />
        </div>

        <div className="form-group">
          <label>Employed</label>
          <div className="employed">
            <label>
              <input id="employed" type="checkbox" {...register("employed")} />
            </label>
          </div>
        </div>

        <div id="color" className="form-group">
          <label>Favorite Color</label>
          <select id="favoriteColor" {...register("favoriteColor")}>
            <option value="green">Green</option>
            <option value="black">Black</option>
            <option value="yellow">Yellow</option>
            <option value="white">white</option>
          </select>
        </div>

        <div className="form-group">
          <label>Sauses</label>
          <div className="checkbox-group checkboxes">
            <label>
              <input type="checkbox" {...register("sauses.Ketchup")} />
              Ketchup
            </label>
            <label>
              <input type="checkbox" {...register("sauses.Mustard")} />
              Mustard
            </label>
            <label>
              <input type="checkbox" {...register("sauses.Mayonnaise")} />
              Mayonnaise
            </label>
            <label>
              <input type="checkbox" {...register("sauses.Guacamole")} />
              Guacamole
            </label>
          </div>
        </div>

        <div className="form-group">
          <label>Best Stooge</label>
          <div id="radio" className="radio-group stooges">
            <label>
              <input
                type="radio"
                {...register("bestStooge")}
                value="larry"
                defaultChecked
              />
              Larry
            </label>
            <label>
              <input type="radio" {...register("bestStooge")} value="moe" />
              Moe
            </label>
            <label>
              <input type="radio" {...register("bestStooge")} value="curly" />
              Curly
            </label>
          </div>
        </div>

        <div className="form-group">
          <label>Note</label>
          <textarea maxLength={100} id="note" {...register("note")}></textarea>
        </div>

        <div disabled className="button-container">
          <button disabled={!isDirty} className="submit-button" type="submit">
            Submit
          </button>
          <button
            onClick={() => reset()}
            className=" reset-button"
            type="button"
            disabled={!isDirty}
          >
            Reset
          </button>
        </div>
      </form>
      <div className="datalist">
        <p>Firstname: {watchFields[0]}</p>
        <p>LastName: {watchFields[1]}</p>
        <p>Age: {watchFields[2]}</p>
        <p>Employed: {watchFields[3]}</p>
        <p>Color: {watchFields[4]}</p>
        <p>
          Sausec: {watchFields[5] ? "ketchup ," : ""}
          {watchFields[6] ? "Mustard ," : ""}{" "}
          {watchFields[7] ? "mayonesse ," : ""}{" "}
          {watchFields[8] ? "Guacamole" : ""}
        </p>
        <p>Best Stooge: {watchFields[9]}</p>
        <p>Note: {watchFields[10]}</p>
      </div>
    </div>
  );
};

export default Form;
